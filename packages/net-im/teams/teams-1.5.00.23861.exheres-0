# Copyright 2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop

SUMMARY="Microsoft Teams"
DESCRIPTION="
Microsoft Teams for Linux is your chat-centered workspace in Office 365.
"
HOMEPAGE="https://teams.microsoft.com/downloads"
DOWNLOADS="
    listed-only:
        platform:amd64? ( https://packages.microsoft.com/repos/ms-teams/pool/main/t/teams/${PN}_${PV}_amd64.deb )
"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( platform: amd64 )
"

DEPENDENCIES="
    run:
        dev-libs/at-spi2-atk
        dev-libs/at-spi2-core
        dev-libs/atk
        dev-libs/expat
        dev-libs/glib:2
        dev-libs/libsecret:1
        dev-libs/nspr
        dev-libs/nss
        net-print/cups
        sys-apps/dbus
        sys-apps/util-linux
        sys-sound/alsa-lib
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXcomposite
        x11-libs/libXcursor
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libXi
        x11-libs/libxkbfile
        x11-libs/libXrandr
        x11-libs/libXrender
        x11-libs/libXScrnSaver
        x11-libs/libXtst
        x11-libs/pango
"

WORK=${WORKBASE}

src_unpack() {
    default

    edo tar xf data.tar.xz
}

src_test() {
    :
}

src_install() {
    insinto /usr/
    doins -r usr/share

    edo chmod 0755 "${IMAGE}"/usr/share/${PN}/${PN}
    dodir /usr/$(exhost --target)/bin
    dosym /usr/share/${PN}/${PN} /usr/$(exhost --target)/bin/${PN}
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
}

